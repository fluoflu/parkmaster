<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Car;
use App\User;

class PageController extends Controller
{
    public function index() {
        $cars = Car::get_parked_cars_with_user_names();
        $per_page = 5;
        $page = Paginator::resolveCurrentPage();
        $currentPage = array_slice($cars, ($page*$per_page-$per_page), $per_page, true);
        $paginator = new Paginator($currentPage, count($cars),5, $page);
        return view('pages.parking')->with('cars', $paginator);
    }
    // all clients page
    public function clients() {
        $cars = Car::get_cars_with_user_names();
        return view('pages.clients')->with('cars', $cars);
    }

    // select a single user from the menu
    public function select_user(Request $request){

        if ($request->isMethod('POST')) {
            $uid = $request->user;
            return redirect('/user/'.$uid);
        }
        else {
            $users = User::get_all();
            return view('pages.select')->with('users', $users);
        }
    }

    public function user_info($user_id) {
        if ($user_id == 'create'){
            return view('register.create-user');
        }
        elseif($user_id == 'delete'){
            $users = User::get_all();
            return view('register.delete-user')->with('users', $users);
        }
        else {
            $user = User::get_user_by_id($user_id);
            $cars = Car::get_cars_by_user_id($user_id);
            return view('pages.user_info')->with(['user'=>$user, 'cars'=> $cars]);
        }
    }

    public function car_info($car_id) {
        $car = Car::get_car_and_owner_by_car_id($car_id);
        return view('pages.car_info')->with('car', $car);
    }
}
