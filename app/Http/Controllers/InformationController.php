<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Car;
use App\User;

class InformationController extends Controller
{

    public function index(){
        return(view('register.create-user'));
    }

    // validation for creating new user or updating one
    protected function validate_user(Request $request, $user_id=NULL){
        $rules = [
            'name' => 'min:3|regex:/^[\pL\s\-]+$/u',
            'gender' => 'required',
            'phone' => 'required|numeric|unique:users,phone',
            'cars'=> 'integer|required|min:1'
        ];
        if($request->isMethod('PATCH')){
            $rules = [
                'name' => 'min:3|regex:/^[\pL\s\-]+$/u',
                'gender' => 'required',
                'phone' => 'required|numeric|unique:users,phone,'.$user_id,
                'cars'=> 'integer|required|min:1'
            ];
        }
        $rus_messages = [
            'required' => 'Это поле должно быть заполнено',
            'integer'=> 'Только целые числа',
            'numeric' => 'Только цифры',
            'name.regex' => 'Только буквы и пробелы',
            'name.min' => 'В имене требуется хотя бы 3 буквы',
            'unique' => 'Такой номер уже существует',
            'cars.min'=> 'Не меньше одной машины',
        ];
        $this->validate($request, $rules, $rus_messages);
    }
    // validation for creating car or updating one from the car info page
    protected function validate_car(Request $request, $car_id=NULL){
        $rules = [
            'brand' => 'required|alpha|max:255',
            'model' => 'required|alpha_num|max:255',
            'color' => 'required|alpha|max:255',
            'regnum' => 'required|alpha_num|unique:cars,regnum|max:255',
            'is_parked' => 'required',
        ];
        if ($request->method() == "PATCH") {
            $rules = [
                'brand' => 'required|alpha|max:255',
                'model' => 'required|alpha_num|max:255',
                'color' => 'required|alpha|max:255',
                'regnum' => 'required|alpha_num|max:255|unique:cars,regnum,'.$car_id,
                'is_parked' => 'required',
            ];
        }
        $rus_messages = [
            'required' => 'Это поле должно быть заполнено',
            'unique' => 'Машина с этим номер уже зарегистрирована',
            'alpha' => 'Можно использовать только буквы',
            'alpha_num' => 'Можно использовать только буквы и цифры',
            'max' => 'Не больше 255 символов'
        ];
        $this->validate($request, $rules, $rus_messages);
    }
    // validation for updating cars from the user info page
    protected function validate_cars(Request $request, $car_id, $i){
        $rules= [
            'brand'.$i => 'required|alpha|max:255',
            'model'.$i => 'required|alpha_num|max:255',
            'color'.$i => 'required|alpha|max:255',
            'regnum'.$i => 'required|alpha_num|max:255|unique:cars,regnum,'.$car_id,
            'is_parked'.$i => 'required'
        ];
        $rus_messages = [
            'required' => 'Это поле должно быть заполнено',
            'unique' => 'Машина с этим номер уже зарегистрирована',
            'alpha' => 'Можно использовать только буквы',
            'alpha_num' => 'Можно использовать только буквы и цифры',
            'max' => 'Не больше 255 символов'
        ];
        $this->validate($request, $rules, $rus_messages);
    }
    // validation for creating new cars from the user info page
    protected function validate_new_car(Request $request, $i=NULL)
    {
        $rules = [
            'brand-new' => 'required|alpha|max:255',
            'model-new' => 'required|alpha_num|max:255',
            'color-new' => 'required|alpha|max:255',
            'regnum-new' => 'required|alpha_num|unique:cars,regnum|max:255',
            'is_parked-new' => 'required'
        ];
        if ($request->isMethod("POST")) {
            $rules = [
                'brand' . $i => 'required|alpha|max:255',
                'model' . $i => 'required|alpha_num|max:255',
                'color' . $i => 'required|alpha|max:255',
                'regnum' . $i => 'required|alpha_num|unique:cars,regnum|max:255',
                'is_parked' . $i => 'required'
            ];
        }
        $rus_messages = [
            'required' => 'Это поле должно быть заполнено',
            'unique' => 'Машина с этим номер уже зарегистрирована',
            'alpha' => 'Можно использовать только буквы',
            'alpha_num' => 'Можно использовать только буквы и цифры',
            'max' => 'Не больше 255 символов'
        ];
        $this->validate($request, $rules, $rus_messages);
    }

    public function create_user(Request $request){
        $this->validate_user($request);
        User::insert([
            'name'=> $request['name'],
            'gender'=> $request['gender'],
            'phone'=> $request['phone'],
            'address'=> $request['address'],
            'cars'=> $request['cars']
        ]);
        $id = User::get_id_by_phone($request['phone']);
        return redirect('/user/'.$id);
    }

    // Create cars from the user info page
    public function create_cars(Request $request, $user_id, $car_amount){
        if($request->isMethod("POST")){
            // first validate all fields
            for($i=0; $i < $car_amount; $i++) {
                $this->validate_new_car($request, $i);
            }
            // if all fields have passed validation, initiate db query
            for ($i=0; $i < $car_amount; $i++){
                Car::insert([
                    'brand' => $request['brand'.$i],
                    'model' => $request['model'.$i],
                    'color' => $request['color'.$i],
                    'regnum' => $request['regnum'.$i],
                    'is_parked' => $request['is_parked'.$i],
                    'user_id' => $user_id
                ]);
            }
        }
        return redirect('/user/'.$user_id);
    }
    // Edit from the user info page
    public function edit_user(Request $request, $user_id){
        if ($request->isMethod('PATCH')){
            $this->validate_user($request, $user_id);
            User::update_by_user_id([
                'user_id'=> $user_id,
                'name'=> $request['name'],
                'gender'=> $request['gender'],
                'phone'=> $request['phone'],
                'address'=> $request['address']
            ]);
            return redirect('/user/'.$user_id);
        }
        elseif ($request->isMethod('GET')) {
            $user = User::get_user_by_id($user_id);
            return view('register.edit-user')->with('user', $user);
        }
    }

    // edit from the car info page
    public function edit_car(Request $request, $car_id){
        if($request->isMethod("PATCH")) {
            $this->validate_car($request, $car_id);
            Car::update_by_car_id([
                'car_id' => $car_id,
                'brand'=> $request['brand'],
                'model'=> $request['model'],
                'color'=> $request['color'],
                'regnum'=> $request['regnum'],
                'is_parked'=> $request['is_parked']
            ]);
            return redirect('/car/'.$car_id);
        }
        elseif ($request->isMethod("GET")){
            $car = Car::get_car_by_car_id($car_id);
            return view('register.edit-car')->with('car', $car);
        }
    }
    // edit car info or add a new car from the user page
    public function edit_cars(Request $request, $user_id, $car_amount){
        // check the input and update the db only if the request method is patch
        if($request->isMethod("PATCH")) {
            // get the existing car data from the user, if it differs from the input then validate and update
            $user_cars = Car::get_cars_by_user_id($user_id);
            for ($i = 0; $i < $car_amount; $i++) {
                if (!
                ($user_cars[$i]->brand == $request['brand' . $i] &&
                    $user_cars[$i]->model == $request['model' . $i] &&
                    $user_cars[$i]->color == $request['color' . $i] &&
                    $user_cars[$i]->regnum == $request['regnum' . $i] &&
                    $user_cars[$i]->is_parked == $request['is_parked' . $i])
                ) {
                    $this->validate_cars($request, $user_cars[$i]->id, $i);
                    Car::update_by_car_id([
                        'car_id' => $user_cars[$i]->id,
                        'brand' => $request['brand' . $i],
                        'model' => $request['model' . $i],
                        'color' => $request['color' . $i],
                        'regnum' => $request['regnum' . $i],
                        'is_parked' => $request['is_parked' . $i]
                    ]);
                }
            }
            // check the data from new car field, if it's not empty, validate the input and create a car
            if (!(
                empty($request['brand-new']) &&
                empty($request['model-new']) &&
                empty($request['color-new']) &&
                empty($request['regnum-new']) &&
                empty($request['is_parked-new'])
            )) {
                $this->validate_new_car($request);
                Car::insert([
                    'brand' => $request['brand-new'],
                    'model' => $request['model-new'],
                    'color' => $request['color-new'],
                    'regnum' => $request['regnum-new'],
                    'is_parked' => $request['is_parked-new'],
                    'user_id' => $user_id
                ]);
                // update the car amount in the user table
                User::update_car_amount([
                    'user_id'=> $user_id,
                    'car_amount'=> $car_amount + 1
                ]);
            }
        }
        return redirect('user/'.$user_id);
    }

    // for the index parking page
    public function car_left($car_id){
        Car::update_parking_status([
            'car_id'=> $car_id,
            'is_parked' => 0
        ]);
        return redirect('/');
    }

    public function delete_car(Request $request, $car_id){
        if($request->isMethod("DELETE")) {
            // get the user id to update the amount of cars
            $user_id = Car::get_user_id_by_car_id($car_id);
            Car::delete_by_car_id($car_id);
            // get the amount of cars by counting query results to correctly represent the info
            $car_amount = count(Car::get_cars_by_user_id($user_id));
            User::update_car_amount([
                'user_id'=> $user_id,
                'car_amount'=> $car_amount
            ]);
        }
        return redirect('/');
    }

    public function delete_user(Request $request){
        if($request->isMethod("DELETE")) {
            User::delete_by_user_id($request->user);
        }
        return redirect('/user/delete');
    }
}
