<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Car
{
    public static function insert($data){
        DB::table('cars')->insert([
            'brand' => $data['brand'],
            'model' => $data['model'],
            'color' => $data['color'],
            'regnum' => $data['regnum'],
            'is_parked' => $data['is_parked'],
            'user_id' => $data['user_id']
        ]);
    }

    public static function update_by_car_id($data){
        DB::table('cars')->where('id', $data['car_id'])->update([
            'brand'=> $data['brand'],
            'model'=> $data['model'],
            'color'=> $data['color'],
            'regnum'=> $data['regnum'],
            'is_parked'=> $data['is_parked']
        ]);
    }

    public static function update_parking_status($data){
        DB::table('cars')->where('id', $data['car_id'])->update(['is_parked'=>$data['is_parked']]);
    }

    public static function get_car_by_car_id($car_id){
        return DB::table('cars')->where('id', $car_id)->first();
    }

    public static function get_cars_by_user_id($user_id){
        return DB::table('cars')->where('user_id', $user_id)->get();
    }

    public static function get_user_id_by_car_id($car_id){
        return DB::table('cars')->where('id',$car_id)->value('user_id');
    }

    public static function get_parked_cars_with_user_names(){
        return DB::select(DB::raw("SELECT users.id AS user_id, users.name, cars.id AS car_id, cars.brand, cars.model, cars.regnum 
                                            FROM users
                                            LEFT JOIN cars
                                            ON users.id = cars.user_id
                                            WHERE cars.is_parked = 1"));
    }

    public static function get_cars_with_user_names(){
        return DB::table('users')
            ->select('users.id as user_id', 'users.name', 'cars.id as car_id', 'cars.brand', 'cars.model', 'cars.regnum')
            ->leftJoin('cars', 'users.id', '=', 'cars.user_id')
            ->paginate(5);
    }
    public static function get_car_and_owner_by_car_id($car_id){
        return DB::table('cars')
            ->select('cars.*', 'users.name')
            ->leftJoin('users','cars.user_id','=','users.id')
            ->where('cars.id', $car_id)
            ->first();
    }
    public static function delete_by_car_id($car_id){
        DB::table('cars')->where('id', $car_id)->delete();
    }
}
