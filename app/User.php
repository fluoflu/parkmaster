<?php

namespace App;

use Illuminate\Support\Facades\DB;

class User
{
    public static function insert($data){
        DB::table('users')->insert([
            'name'=> $data['name'],
            'gender'=> $data['gender'],
            'phone'=> $data['phone'],
            'address'=> $data['address'],
            'cars'=> $data['cars']
        ]);
}
    public static function get_id_by_phone($phone){
        return DB::table('users')->where('phone', $phone)->value('id');
    }

    public static function get_user_by_id($user_id){
        return DB::table('users')->where('id', $user_id)->first();
    }

    public static function get_all(){
        return DB::table('users')->get();
    }

    public static function update_by_user_id($data){
        DB::table('users')->where('id', $data['user_id'])->update([
            'name'=> $data['name'],
            'gender'=> $data['gender'],
            'phone'=> $data['phone'],
            'address'=> $data['address']
        ]);
    }

    public static function update_car_amount($data){
        DB::table('users')->where('id', $data['user_id'])->update(['cars' => $data['car_amount']]);
    }

    public static function delete_by_user_id($user_id){
        DB::table('users')->where('id', $user_id)->delete();
    }

}
