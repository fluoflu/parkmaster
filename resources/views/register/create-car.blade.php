@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Добавить машину клиенту {{$name}}</strong></div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{url('/user/'.$user_id.'/new-car')}}">
                            {{ csrf_field() }}
                            <div class="container">
                                <br>
                                <div class="form-group{{ $errors->has('brand') ? ' has-error' : '' }}">
                                    <label for="brand" class="col-md-4 control-label">Марка</label>
                                    <div class="col-md-6">
                                        <input id="brand" type="text" class="form-control" name="brand" value="{{ old('brand') }}">

                                        @if ($errors->has('brand'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('brand') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('model') ? ' has-error' : '' }}">
                                    <label for="model" class="col-md-4 control-label">Модель</label>
                                    <div class="col-md-6">
                                        <input id="model" type="text" class="form-control" name="model" value="{{ old('model') }}">

                                        @if ($errors->has('model'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('model') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
                                    <label for="color" class="col-md-4 control-label">Цвет</label>
                                    <div class="col-md-6">
                                        <input id="color" type="text" class="form-control" name="color" value="{{old('color')}}">
                                        @if ($errors->has('color'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('color') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('regnum') ? ' has-error' : '' }}">
                                    <label for="regnum" class="col-md-4 control-label">Гос Номер РФ</label>
                                    <div class="col-md-6">
                                        <input id="regnum" type="text" class="form-control" name="regnum" value="{{old('regnum')}}">
                                        @if ($errors->has('regnum'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('regnum') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('is_parked') ? ' has-error' : '' }}">
                                    <label for="is_parked" class="col-md-4 control-label">Парковка</label>
                                    <div class="col-md-6">
                                        <select id="is_parked" class="form-control" name="is_parked">
                                            <option value=1>Припаркована</option>
                                            <option value=0>Выехала</option>
                                        </select>
                                        @if ($errors->has('is_parked'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('is_parked') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Зарегистрировать
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection