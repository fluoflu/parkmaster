@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if(count($users)<1)
                        <h3>У вас нет клиентов!</h3><br>
                        <h3><a href="{{url('/user/create')}}">Вот, создайте первого клиента!</a></h3>
                    @else
                    <div class="panel-heading">Выберите пользователя</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{url('/user/delete')}}">
                            {{ csrf_field() }}
                            {{ method_field("DELETE") }}
                            <div class="form-group">
                                <label for="user" class="col-md-4 control-label">Клиент</label>
                                <div class="col-md-6">
                                    <select id="user" class="form-control" name="user">
                                        @foreach ($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-danger">Удалить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection