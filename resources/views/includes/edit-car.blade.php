<div class="panel-heading"><h3>Автомобили </h3></div>
<form class="form-horizontal" method="POST" action="{{url('/car/edit-many/'.$user->id.'-'.$user->cars)}}">
    {{ csrf_field() }}
    {{ method_field("PATCH") }}
@for($i=0; $i < count($cars); $i++)
    <div class="panel-heading"><strong>Автомобиль {{$i+1}}</strong></div>
    <div class="container">
        <div class="form-group{{ $errors->has('brand'.$i) ? ' has-error' : '' }}">
            <label for="{{'brand'.$i}}" class="col-md-4 control-label">Марка</label>
            <div class="col-md-6">
                <input id="{{'brand'.$i}}" type="text" class="form-control" name="{{'brand'.$i}}" value="{{ $cars[$i]->brand }}">

                @if ($errors->has('brand'.$i))
                    <span class="help-block">
                        <strong>{{ $errors->first('brand'.$i) }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('model'.$i) ? ' has-error' : '' }}">
            <label for="{{'model'.$i}}" class="col-md-4 control-label">Модель</label>
            <div class="col-md-6">
                <input id="{{'model'.$i}}" type="text" class="form-control" name="{{'model'.$i}}" value="{{$cars[$i]->model}}">

                @if ($errors->has('model'.$i))
                    <span class="help-block">
                        <strong>{{ $errors->first('model'.$i) }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('color'.$i) ? ' has-error' : '' }}">
            <label for="{{'color'.$i}}" class="col-md-4 control-label">Цвет</label>
            <div class="col-md-6">
                <input id="{{'color'.$i}}" type="text" class="form-control" name="{{'color'.$i}}" value="{{ $cars[$i]->color }}">
                @if ($errors->has('color'.$i))
                    <span class="help-block">
                        <strong>{{ $errors->first('color'.$i) }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('regnum'.$i) ? ' has-error' : '' }}">
            <label for="{{'regnum'.$i}}" class="col-md-4 control-label">Гос Номер РФ</label>
            <div class="col-md-6">
                <input id="{{'regnum'.$i}}" type="text" class="form-control" name="{{'regnum'.$i}}" value="{{ $cars[$i]->regnum }}">
                @if ($errors->has('regnum'.$i))
                    <span class="help-block">
                        <strong>{{ $errors->first('regnum'.$i) }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('is_parked'.$i) ? ' has-error' : '' }}">
            <label for="{{'is_parked'.$i}}" class="col-md-4 control-label">Парковка</label>
            <div class="col-md-6">
                <select id="{{'is_parked'.$i}}" class="form-control" name="{{'is_parked'.$i}}">
                        <option value="" disabled></option>
                    @if($cars[$i]->is_parked == 1)
                        <option value=1 selected="selected">Припаркована</option>
                        <option value=0>Выехала</option>
                    @else
                        <option value=1>Припаркована</option>
                        <option value=0 selected="selected">Выехала</option>
                    @endif
                </select>
                @if ($errors->has('is_parked'.$i))
                    <span class="help-block">
                        <strong>{{ $errors->first('is_parked'.$i) }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
@endfor
<div class="panel-heading"><strong>Новый Автомобиль</strong></div>
<div class="container">
    <div class="form-group{{ $errors->has('brand-new') ? ' has-error' : '' }}">
        <label for="brand-new" class="col-md-4 control-label">Марка</label>
        <div class="col-md-6">
            <input id="brand-new" type="text" class="form-control" name="brand-new" value="{{old('brand-new')}}">

            @if ($errors->has('brand-new'))
                <span class="help-block">
                        <strong>{{ $errors->first('brand-new') }}</strong>
                    </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('model-new') ? ' has-error' : '' }}">
        <label for="model-new" class="col-md-4 control-label">Модель</label>
        <div class="col-md-6">
            <input id="model-new" type="text" class="form-control" name="model-new" value="{{ old('model-new') }}">

            @if ($errors->has('model-new'))
                <span class="help-block">
                        <strong>{{ $errors->first('model-new') }}</strong>
                    </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('color-new') ? ' has-error' : '' }}">
        <label for="color-new" class="col-md-4 control-label">Цвет</label>
        <div class="col-md-6">
            <input id="color-new" type="text" class="form-control" name="color-new" value="{{old('color-new')}}">
            @if ($errors->has('color-new'))
                <span class="help-block">
                        <strong>{{ $errors->first('color-new') }}</strong>
                    </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('regnum-new') ? ' has-error' : '' }}">
        <label for="regnum-new" class="col-md-4 control-label">Гос Номер РФ</label>
        <div class="col-md-6">
            <input id="regnum-new" type="text" class="form-control" name="regnum-new" value="{{ old('regnum-new') }}">
            @if ($errors->has('regnum-new'))
                <span class="help-block">
                        <strong>{{ $errors->first('regnum-new') }}</strong>
                    </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('is_parked-new') ? ' has-error' : '' }}">
        <label for="is_parked-new" class="col-md-4 control-label">Парковка</label>
        <div class="col-md-6">
            <select id="is_parked-new" class="form-control" name="is_parked-new">
                    <option value="" selected="selected" disabled></option>
                    <option value=1>Припаркована</option>
                    <option value=0>Выехала</option>
            </select>
            @if ($errors->has('is_parked-new'))
                <span class="help-block">
                        <strong>{{ $errors->first('is_parked-new') }}</strong>
                    </span>
            @endif
        </div>
    </div>
</div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                Сохранить
            </button>
        </div>
    </div>
</form>
