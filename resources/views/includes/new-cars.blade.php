<form class="form-horizontal" method="POST" action="{{url('/user/'.$user->id.'/new-cars-'.$user->cars)}}">
    {{csrf_field()}}
    @for($i=0; $i<$user->cars; $i++)
        <div class="panel-heading"><strong>Новый Автомобиль {{$i+1}}</strong></div>
        <div class="container">
            <div class="form-group{{ $errors->has('brand'.$i) ? ' has-error' : '' }}">
                <label for="{{'brand'.$i}}" class="col-md-4 control-label">Марка</label>
                <div class="col-md-6">
                    <input id="{{'brand'.$i}}" type="text" class="form-control" name="{{'brand'.$i}}" value="{{old('brand'.$i)}}">

                    @if ($errors->has('brand'.$i))
                        <span class="help-block">
                        <strong>{{ $errors->first('brand'.$i) }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('model'.$i) ? ' has-error' : '' }}">
                <label for="{{'model'.$i}}" class="col-md-4 control-label">Модель</label>
                <div class="col-md-6">
                    <input id="{{'model'.$i}}" type="text" class="form-control" name="{{'model'.$i}}" value="{{ old('model'.$i) }}">

                    @if ($errors->has('model'.$i))
                        <span class="help-block">
                        <strong>{{ $errors->first('model'.$i) }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('color'.$i) ? ' has-error' : '' }}">
                <label for="{{'color'.$i}}" class="col-md-4 control-label">Цвет</label>
                <div class="col-md-6">
                    <input id="{{'color'.$i}}" type="text" class="form-control" name="{{'color'.$i}}" value="{{old('color'.$i)}}">
                    @if ($errors->has('color'.$i))
                        <span class="help-block">
                        <strong>{{ $errors->first('color'.$i) }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('regnum'.$i) ? ' has-error' : '' }}">
                <label for="{{'regnum'.$i}}" class="col-md-4 control-label">Гос Номер РФ</label>
                <div class="col-md-6">
                    <input id="{{'regnum'.$i}}" type="text" class="form-control" name="{{'regnum'.$i}}" value="{{ old('regnum'.$i) }}">
                    @if ($errors->has('regnum'.$i))
                        <span class="help-block">
                        <strong>{{ $errors->first('regnum'.$i) }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('is_parked'.$i) ? ' has-error' : '' }}">
                <label for="{{'is_parked'.$i}}" class="col-md-4 control-label">Парковка</label>
                <div class="col-md-6">
                    <select id="{{'is_parked'.$i}}" class="form-control" name="{{'is_parked'.$i}}">
                        <option value="" selected="selected" disabled></option>
                        <option value=1>Припаркована</option>
                        <option value=0>Выехала</option>
                    </select>
                    @if ($errors->has('is_parked'.$i))
                        <span class="help-block">
                        <strong>{{ $errors->first('is_parked'.$i) }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    @endfor
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                Сохранить
            </button>
        </div>
    </div>
</form>