@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if (count($cars) < 1)
                        <h3>У вас нет ни клиентов, ни машин!</h3><br>
                        <h3><a href="{{url('/user/create')}}">Вот, создайте первого клиента!</a></h3>
                    @else
                        <div class="panel-heading"><h3>Все клиенты</h3></div>
                            <table class="table table-striped">
                                @foreach($cars as $car)
                                    <tr>
                                        <td><a href="{{url('/user/'.$car->user_id)}}">{{$car->name}}</a></td>
                                        <td><a href="{{url('/car/'.$car->car_id)}}"> {{$car->brand.' '.$car->model}} </a></td>
                                        <td>{{$car->regnum}}</td>
                                        <td>
                                            <a href="{{url('/car/'.$car->car_id.'/edit')}}">
                                                <button type="button"  class="btn btn-default btn-sm">✎</button>
                                            </a>
                                        </td>
                                        <td>
                                            <form type="form-group" method="POST" action="{{url('/car/'.$car->car_id)}}">
                                                {{ csrf_field() }}
                                                {{ method_field("DELETE") }}
                                                <button type="submit" class="btn-outline-danger">✖</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    {{ $cars->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection