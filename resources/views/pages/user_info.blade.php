@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if(!isset($user->name))
                        <h3>Данного пользователя не существует</h3>
                    @else
                        <div class="panel-heading"><h3>Клиент</h3></div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{url('/user/'.$user->id.'/edit')}}">
                                {{ csrf_field() }}
                                {{ method_field("PATCH") }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">ФИО</label>
                                    <div class="col-md-4">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}">

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                    <label for="gender" class="col-md-4 control-label">Пол</label>
                                    <div class="col-md-4">
                                        <select id="gender" class="form-control" name="gender">
                                            <option value="male">Мужчина</option>
                                            @if($user->gender == 'female')
                                                <option value="female" selected="selected">Женщина</option>
                                            @else
                                                <option value="female">Женщина</option>
                                            @endif
                                        </select>

                                        @if ($errors->has('gender'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="col-md-4 control-label">Телефон</label>
                                    <div class="col-md-4">
                                        <input id="phone" type="text" class="form-control" name="phone" value="{{$user->phone}}">
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address" class="col-md-4 control-label">Адрес</label>
                                    <div class="col-md-4">
                                        <input id="address" type="text" class="form-control" name="address" value="{{$user->address}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Сохранить
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @if(count($cars) < 1 )
                            @include('includes.new-cars')
                        @else
                            @include('includes.edit-car')
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection