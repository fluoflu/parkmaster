@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if(!isset($car))
                        <h3>Такой машины не существует</h3>
                    @else
                        <div class="panel-heading"><h3>{{$car->brand.' '.$car->model}}</h3></div>
                        <a>{{$car->regnum}}</a><br>
                        <a>Цвет {{$car->color}}</a><br>
                        <a>Владелец {{$car->name}}</a><br>
                        @if($car->is_parked == 0)
                            <a>Выехала</a><br>
                        @else
                            <a>На парковке</a><br>
                        @endif
                        <a href="{{url('/car/'.$car->id.'/edit')}}" class="btn btn-default">Редактировать</a><br>
                        <form class="form-group" method="POST" action="{{url('/car/'.$car->id)}}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">Удалить</button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection