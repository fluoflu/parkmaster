@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Выберите пользователя</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{url('/user')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="user" class="col-md-4 control-label">Клиент</label>
                                <div class="col-md-6">
                                    <select id="user" class="form-control" name="user">
                                        @foreach ($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Выбрать
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection