<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can Information web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Main pages
Route::get('/', 'PageController@index');
Route::get('/all', 'PageController@clients');
Route::get('/user', 'PageController@select_user');
Route::post('/user', 'PageController@select_user');
Route::get('/user/{user_id}', 'PageController@user_info');
Route::get('/car/{car_id}', 'PageController@car_info');
// Creation requests
Route::post('/user/create','InformationController@create_user');
Route::post('/user/{user_id}/new-cars-{car_amount}','InformationController@create_cars');
// Edit pages
Route::get('/user/{user_id}/edit', 'InformationController@edit_user');
Route::patch('/user/{user_id}/edit', 'InformationController@edit_user');
Route::get('/car/{car_id}/edit', 'InformationController@edit_car');
Route::patch('/car/{car_id}/edit', 'InformationController@edit_car');
Route::patch('/car/edit-many/{user_id}-{car_amount}', 'InformationController@edit_cars');
Route::patch('/{car_id}','InformationController@car_left');
// Delete data
Route::delete('/car/{car_id}', 'InformationController@delete_car');
Route::delete('/user/delete', 'InformationController@delete_user');